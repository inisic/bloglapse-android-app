package com.bloglapse.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bloglapse.R;
import com.bloglapse.model.ListAdapter;
import com.bloglapse.model.ListItem;
import com.bloglapse.services.ListManager;

import java.util.List;


public class MainFragment extends Fragment {

    private static final String TAG = MainFragment.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private List<ListItem> itemList;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View homeFragmentView = inflater.inflate(R.layout.fragment_main, container, false);
        itemList = ListManager.getInstance().getList(30);

        return homeFragmentView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.customRecyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        ListAdapter listAdapter = new ListAdapter(itemList, R.layout.list_row, getActivity());
        mRecyclerView.setAdapter(listAdapter);


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
