package com.bloglapse.model;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bloglapse.R;
import com.bloglapse.activity.WebViewActivity;

import java.util.List;

/**
 * Created by Admin on 08.06.2015.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private static final String TAG = ListAdapter.class.getSimpleName();
    private final int rowLayout;
    private static List<ListItem> itemsList;
    private Context mContext;

    public ListAdapter(List<ListItem> itemsList, int rowLayout, Context context) {
        this.itemsList = itemsList;
        this.rowLayout = rowLayout;
        this.mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {


        ListItem newsItem = itemsList.get((i));

        //viewHolder.itemView.setTag(newsItem);

        if (newsItem != null) {
            viewHolder.listTitle.setText(newsItem.getTitle());
            viewHolder.listDescription.setText(newsItem.getDescription());
            viewHolder.listDate.setText(Long.toString(newsItem.getDate()));
        }
    }

    @Override
    public int getItemCount() {
        return itemsList == null ? 0 : itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView listTitle, listDescription, listDate;
        private LinearLayout itemContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            listTitle = (TextView) itemView.findViewById(R.id.listTitle);
            listDescription = (TextView) itemView.findViewById(R.id.listDescription);
            listDate = (TextView) itemView.findViewById(R.id.listDate);
            itemContainer = (LinearLayout) itemView.findViewById(R.id.itemContainer);


            if (itemContainer != null) {

                itemContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startViewActivity(v);
                    }
                });

            }
        }

        public void startViewActivity(View v) {

            ListItem selectedItem = itemsList.get(getPosition());
            Intent intent = new Intent(v.getContext(), WebViewActivity.class);
            intent.putExtra("url", selectedItem.getUrl());
            v.getContext().startActivity(intent);
        }
    }

}


