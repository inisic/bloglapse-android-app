package com.bloglapse.model;

/**
 * Created by root on 11/2/15.
 */
public class ListItem {
    private String title;
    private String description;
    private long date;
    private String url;

    public ListItem(String title, String description, long date, String url) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
