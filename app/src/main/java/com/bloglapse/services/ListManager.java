package com.bloglapse.services;

import com.bloglapse.model.ListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 08.06.2015.
 */
public class ListManager {

    private static final String TAG = ListManager.class.getSimpleName();

    private static ListManager mInstance;


    public static ListManager getInstance() {
        if (mInstance == null) {
            mInstance = new ListManager();
        }
        return mInstance;
    }

    public List<ListItem> getList(int listSize) {

        ArrayList<ListItem> listItemList = new ArrayList<>();

        for (int i = 0; i <= listSize; i++) {
            ListItem item = new ListItem("Title " + i, "Description " + i, i * 1000, "http://www.google.com");
            listItemList.add(item);
        }

        return listItemList;

    }


}
